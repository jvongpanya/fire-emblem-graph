# About

Takes an Excel workbook containing Fire Emblem character relationship data, and generates a graph representing the relationships, and saves it as JSON. 
Currently, produces a flat graph, so a node's related characters are not references to other nodes. This is to minimize the JSON payload.

# Usage

dotnet FireEmblemGraph.dll { input path to Excel workbook } { output path } {graphType, where 0=Awakening, 1=Echoes, 2=Three Houses}

For example, if you have an excel sheet representing Fire Emblem Awakening:
dotnet FireEmblemGraph.dll "C:\Users\USER\Downloads\Fire Emblem Awakening Relationships.xlsx" "C:\Users\USER\Downloads\FEAwakening.json" 0

# Excel File Format

Sample sheet: http://bit.ly/2qzq4Jt

* This will iterate through each sheet in an Excel workbook. 
* One row represents a character and their supporting characters.
* The first column in each sheet should be the name of the character to add relationships to. 
* Depending on the game selected, the following columns should be character metadata, or the supporting characters. 
* The last column of each row should be "--" to indicate that there are no more relationships. 
* The characters and relationships added will be unique.

Workbook information was pulled from https://fireemblemwiki.org/, which is why the workbooks are formatted in this way.