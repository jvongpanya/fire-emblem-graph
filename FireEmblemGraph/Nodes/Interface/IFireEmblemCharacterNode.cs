﻿using FireEmblemGraph.Domain.Interface;
using System.Collections.Generic;

namespace FireEmblemGraph.Nodes.Interface
{
    public interface IFireEmblemCharacterNode
    {
        ICharacterData CharacterData { get; }
        IList<IFireEmblemCharacterNode> Relationships { get; }
        bool AddRelationship(IFireEmblemCharacterNode fireEmblemCharacterNode);
        bool HasRelatedCharacter(string name);
    }
}
