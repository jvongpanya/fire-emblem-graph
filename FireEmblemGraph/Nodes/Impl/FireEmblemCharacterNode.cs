﻿using FireEmblemGraph.Domain.Interface;

namespace FireEmblemGraph.Nodes.Impl
{
    public class FireEmblemCharacterNode : FireEmblemCharacterNodeBase
    {
        public FireEmblemCharacterNode(ICharacterData characterData): base(characterData) { }
    }
}
