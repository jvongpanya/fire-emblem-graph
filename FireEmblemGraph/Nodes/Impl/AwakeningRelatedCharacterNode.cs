﻿using FireEmblemGraph.Domain.Interface;

namespace FireEmblemGraph.Nodes.Impl
{
    public class AwakeningRelatedCharacterNode : FireEmblemCharacterNodeBase
    {
        public bool IsRomantic { get; set; }
        public bool IsParentChild { get; set; }
        public bool IsNonRomantic { get; set; }
        public bool IsSibling { get; set; }

        public AwakeningRelatedCharacterNode(ICharacterData characterData): base(characterData) { }
    }
}
