﻿using FireEmblemGraph.Domain.Impl;
using FireEmblemGraph.Domain.Interface;

namespace FireEmblemGraph.Nodes.Impl
{
    public class FireEmblemThreeHousesCharacterNode : FireEmblemCharacterNodeBase
    {
        public FireEmblemThreeHousesCharacterNode(FireEmblemThreeHousesCharacterData characterData) : base(characterData) { }
    }
}
