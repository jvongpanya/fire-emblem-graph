﻿using FireEmblemGraph.Domain.Interface;
using FireEmblemGraph.Nodes.Interface;
using System.Collections.Generic;
using System.Linq;

namespace FireEmblemGraph.Nodes.Impl
{
    public abstract class FireEmblemCharacterNodeBase : IFireEmblemCharacterNode
    {
        public ICharacterData CharacterData { get; private set; }
        public IList<IFireEmblemCharacterNode> Relationships { get; private set; }

        protected FireEmblemCharacterNodeBase(ICharacterData characterData)
        {
            Relationships = new List<IFireEmblemCharacterNode>();
            CharacterData = characterData;
        }

        public bool AddRelationship(IFireEmblemCharacterNode fireEmblemCharacterNode)
        {
            var exists = Relationships.Any(n => n.CharacterData.Name.Equals(fireEmblemCharacterNode.CharacterData.Name));
            if (exists)
                return false;
            Relationships.Add(fireEmblemCharacterNode);
            return true;
        }

        public bool HasRelatedCharacter(string name)
        {
            return Relationships.Any(r => r.CharacterData.Name.Equals(name));
        }
    }
}
