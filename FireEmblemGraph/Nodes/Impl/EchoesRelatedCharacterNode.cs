﻿using FireEmblemGraph.Domain.Interface;

namespace FireEmblemGraph.Nodes.Impl
{
    public class EchoesRelatedCharacterNode : FireEmblemCharacterNodeBase
    {
        public bool HasConversation { get; set; }
        public bool IsDLC { get; set; }
        public bool HasBond { get; set; }

        public EchoesRelatedCharacterNode(ICharacterData characterData): base(characterData) { }
    }
}
