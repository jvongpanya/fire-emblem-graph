﻿using FireEmblemGraph.Domain.Impl;
using FireEmblemGraph.GraphBuilder.Impl;
using System;

namespace FireEmblemGraph
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 3)
                throw new ArgumentException("Needs three arguments: {inputPath} {outputPath} {graphType, where 0=Awakening, 1=Echoes, 2=Three Houses}");
            var inputPath = args[0];
            var outputPath = args[1];
            var graphType = args[2];
            Console.WriteLine("Creating JSON");
            var graphBuilderFactory = new FireEmblemExcelGraphBuilderFactory(inputPath);
            var graphTypeEnum = GetGameFromStringIndex(graphType);
            Console.WriteLine($"Game: { graphTypeEnum.ToString() }");
            var graphBuilder = graphBuilderFactory.Create(graphTypeEnum);
            using (var saver = new StringSaver(graphBuilder))
            {
                saver.SaveString(outputPath);
            }
        }

        private static EnumFireEmblemGames GetGameFromStringIndex(string enumStringIndex)
        {
            var index = int.Parse(enumStringIndex);
            return (EnumFireEmblemGames)index;
        }
    }
}
