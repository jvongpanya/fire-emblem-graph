﻿using FireEmblemGraph.Domain.Impl;
using FireEmblemGraph.Nodes.Impl;
using FireEmblemGraph.Nodes.Interface;
using System;

namespace FireEmblemGraph.GraphBuilder.Impl
{
    public class ExcelAwakeningFlatGraphBuilder : ExcelFeFlatGraphBuilderBase
    {
        protected override int StartRelationshipsColumn => 2;
        private const string ROMANTIC_SHEET = "Romantic";
        private const string PARENT_CHILD_SHEET = "Parent-Child";
        private const string NON_ROMANTIC_SHEET = "Non-Romantic";
        private const string SIBLING_SHEET = "Sibling";

        public ExcelAwakeningFlatGraphBuilder(string filePath) : base(filePath)
        {
            _sheets.Add(ROMANTIC_SHEET);
            _sheets.Add(PARENT_CHILD_SHEET);
            _sheets.Add(NON_ROMANTIC_SHEET);
            _sheets.Add(SIBLING_SHEET);
        }

        protected override IFireEmblemCharacterNode CreateSupportNode_Internal(string name, int currentRow, string sheet)
        {
            var newCharacterData = new FireEmblemCharacterData(name);
            var newCharacter = new AwakeningRelatedCharacterNode(newCharacterData);
            return newCharacter;
        }

        protected override void UpdateSupportCharacter(IFireEmblemCharacterNode characterNode, string sheet)
        {
            var echoesNode = characterNode as AwakeningRelatedCharacterNode;
            if (echoesNode == null)
                throw new ArgumentException($"The node provided, {nameof(characterNode)}, is not supported");

            if (sheet.Equals(ROMANTIC_SHEET))
                echoesNode.IsRomantic = true;

            if (sheet.Equals(PARENT_CHILD_SHEET))
                echoesNode.IsParentChild = true;

            if (sheet.Equals(NON_ROMANTIC_SHEET))
                echoesNode.IsNonRomantic = true;

            if (sheet.Equals(SIBLING_SHEET))
                echoesNode.IsSibling = true;
        }
    }
}
