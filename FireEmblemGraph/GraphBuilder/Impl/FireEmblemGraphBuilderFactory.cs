﻿using FireEmblemGraph.Domain.Impl;
using FireEmblemGraph.GraphBuilder.Interface;
using System;

namespace FireEmblemGraph.GraphBuilder.Impl
{
    public class FireEmblemExcelGraphBuilderFactory : IFireEmblemGraphBuilderFactory
    {
        private string _filePath;

        public FireEmblemExcelGraphBuilderFactory(string filePath)
        {
            _filePath = filePath;
        }

        public IFireEmblemCharacterGraphBuilder Create(EnumFireEmblemGames game)
        {
            switch(game)
            {
                case EnumFireEmblemGames.Awakening:
                    return new ExcelAwakeningFlatGraphBuilder(_filePath);
                case EnumFireEmblemGames.Echoes:
                    return new ExcelEchoesFlatGraphBuilder(_filePath);
                case EnumFireEmblemGames.ThreeHouses:
                    return new ExcelFeThreeHousesFlatGraphBuilder(_filePath);
                default:
                    throw new ArgumentException($"The game provided, { game }, is not supported");
            }
        }
    }
}
