﻿using FireEmblemGraph.Graphs.Interface;
using FireEmblemGraph.Nodes.Interface;
using System;
using System.Linq;

namespace FireEmblemGraph.GraphBuilder.Impl
{
    public abstract class ExcelFeFlatGraphBuilderBase : ExcelFeGraphBuilderBase
    {
        protected ExcelFeFlatGraphBuilderBase(string filePath) : base(filePath)
        {
        }

        /// <summary>
        /// Generates supports without any relationships by creating new support character objects, instead of referencing existing characters from the graph.
        /// </summary>
        /// <param name="currentRow"></param>
        /// <param name="columns"></param>
        /// <param name="sheet"></param>
        /// <param name="graph"></param>
        /// <param name="mainCharacter"></param>
        protected override void ProcessSupport_Internal(int currentRow, int columns, IFireEmblemCharacterGraph graph, IFireEmblemCharacterNode mainCharacter, string sheet)
        {
            ProcessSupport_Flat_Internal(currentRow, columns, sheet, graph, mainCharacter);
        }

        protected virtual void ProcessSupport_Flat_Internal(int currentRow, int columns, string sheet, IFireEmblemCharacterGraph graph, IFireEmblemCharacterNode mainCharacter)
        {

            for (int currentColumn = StartRelationshipsColumn; currentColumn <= columns; currentColumn++) // Starts at third
            {
                try
                {
                    var supportCharacterName = _reader.GetCellStringValue(currentRow, currentColumn, sheet);
                    if (supportCharacterName.Equals(END_OF_RELATIONSHIPS))
                        break;
                    var hasSupport = mainCharacter.HasRelatedCharacter(supportCharacterName);
                    if (!hasSupport)
                    {
                        var newCharacter = CreateSupportNode_Internal(supportCharacterName, currentRow, sheet);
                        mainCharacter.AddRelationship(newCharacter);
                    }
                    var supportCharacter = mainCharacter.Relationships.FirstOrDefault(r => r.CharacterData.Name.Equals(supportCharacterName));
                    UpdateSupportCharacter(supportCharacter, sheet);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error found while processing support character at [{ currentRow }, { currentColumn }]");
                    throw;
                }
            }
        }

        protected virtual IFireEmblemCharacterNode CreateSupportNode_Internal(string name, int currentRow, string sheet)
        {
            return CreateCharacter_Internal(name, currentRow, sheet);
        }
    }
}
