﻿using FireEmblemGraph.Graphs.Interface;
using FireEmblemGraph.Nodes.Interface;
using System;

namespace FireEmblemGraph.GraphBuilder.Impl
{
    public class ExcelFeGraphBuilder : ExcelFeGraphBuilderBase
    {
        public ExcelFeGraphBuilder(string filePath) : base(filePath)
        {
        }

        protected override void UpdateSupportCharacter(IFireEmblemCharacterNode characterNode, string sheet)
        {
            // Do nothing
        }
    }
}
