﻿using FireEmblemGraph.Domain.Impl;
using FireEmblemGraph.Nodes.Impl;
using FireEmblemGraph.Nodes.Interface;
using System;

namespace FireEmblemGraph.GraphBuilder.Impl
{
    public class ExcelEchoesFlatGraphBuilder : ExcelFeFlatGraphBuilderBase
    {
        protected override int StartRelationshipsColumn => 2;
        private const string CONVERSATION_SHEET = "Conversations";
        private const string DLC_SHEET = "DLC";
        private const string BONDS_SHEET = "Bonds";

        public ExcelEchoesFlatGraphBuilder(string filePath) : base(filePath)
        {
            _sheets.Add(CONVERSATION_SHEET);
            _sheets.Add(DLC_SHEET);
            _sheets.Add(BONDS_SHEET);
        }

        protected override IFireEmblemCharacterNode CreateSupportNode_Internal(string name, int currentRow, string sheet)
        {
            var newCharacterData = new FireEmblemCharacterData(name);
            var newCharacter = new EchoesRelatedCharacterNode(newCharacterData);
            return newCharacter;
        }

        protected override void UpdateSupportCharacter(IFireEmblemCharacterNode characterNode, string sheet)
        {
            var echoesNode = characterNode as EchoesRelatedCharacterNode;
            if (echoesNode == null)
                throw new ArgumentException($"The node provided, {nameof(characterNode)}, is not supported");

            if (sheet.Equals(CONVERSATION_SHEET))
                echoesNode.HasConversation = true;

            if (sheet.Equals(DLC_SHEET))
                echoesNode.IsDLC = true;

            if (sheet.Equals(BONDS_SHEET))
                echoesNode.HasBond = true;
        }
    }
}
