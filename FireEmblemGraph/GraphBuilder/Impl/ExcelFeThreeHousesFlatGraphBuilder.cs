﻿using FireEmblemGraph.Domain.Impl;
using FireEmblemGraph.Nodes.Impl;
using FireEmblemGraph.Nodes.Interface;

namespace FireEmblemGraph.GraphBuilder.Impl
{
    public class ExcelFeThreeHousesFlatGraphBuilder : ExcelFeFlatGraphBuilderBase
    {
        private readonly int HOUSE_COLUMN = 2;
        private readonly string SUPPORT_SHEET = "Supports";

        protected override int StartRelationshipsColumn => HOUSE_COLUMN + 1;

        public ExcelFeThreeHousesFlatGraphBuilder(string filePath) : base(filePath)
        {
            _sheets.Add(SUPPORT_SHEET);
        }

        protected override IFireEmblemCharacterNode CreateCharacter_Internal(string name, int currentRow, string sheet)
        {
            var house = GetHouse(sheet, currentRow);
            var newCharacterData = new FireEmblemThreeHousesCharacterData(name, house);
            var newCharacter = new FireEmblemThreeHousesCharacterNode(newCharacterData);
            return newCharacter;
        }

        private string GetHouse(string sheet, int currentRow)
        {
            return _reader.GetCellStringValue(currentRow, HOUSE_COLUMN, sheet);
        }

        protected override void UpdateSupportCharacter(IFireEmblemCharacterNode characterNode, string sheet)
        {
            // Do nothing
        }
    }
}
