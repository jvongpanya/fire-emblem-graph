﻿using FireEmblemGraph.GraphBuilder.Interface;
using Newtonsoft.Json;
using System;
using System.IO;

namespace FireEmblemGraph.GraphBuilder.Impl
{
    public class StringSaver : IDisposable
    {
        IFireEmblemCharacterGraphBuilder _graphBuilder;
        public StringSaver(IFireEmblemCharacterGraphBuilder graphBuilder)
        {
            _graphBuilder = graphBuilder;
        }

        public void Dispose()
        {
            _graphBuilder.Dispose();
        }

        public void SaveString(string savePath)
        {
            try
            {
                var graph = _graphBuilder.BuildGraph();
                using (TextWriter textWriter = File.CreateText(savePath))
                {
                    var serializer = new JsonSerializer();
                    serializer.Formatting = Formatting.Indented;
                    serializer.Serialize(textWriter, graph);
                }
            }
            catch(Exception e)
            {
                throw;
            }
        }
    }
}
