﻿using FireEmblemGraph.Domain.Impl;
using FireEmblemGraph.GraphBuilder.Interface;
using FireEmblemGraph.Graphs.Impl;
using FireEmblemGraph.Graphs.Interface;
using FireEmblemGraph.Nodes.Impl;
using FireEmblemGraph.Nodes.Interface;
using SpreadsheetReader.Implementation;
using SpreadsheetReader.Interface;
using System;
using System.Collections.Generic;

namespace FireEmblemGraph.GraphBuilder.Impl
{
    public abstract class ExcelFeGraphBuilderBase : IFireEmblemCharacterGraphBuilder, IDisposable
    {
        // TODO: Refactor how sheets are initialized
        protected readonly IList<string> _sheets; 
        protected readonly ISpreadsheetReader _reader;
        protected readonly string _filePath;
        protected const string END_OF_RELATIONSHIPS = "--";
        protected virtual int StartRelationshipsColumn => 2;

        protected ExcelFeGraphBuilderBase(string filePath)
        {
            _filePath = filePath;
            _reader = new EpPlusWrapper(_filePath);
            _sheets = new List<string>(); // Sheets must be initialized by implementation. 
        }

        protected ExcelFeGraphBuilderBase(ISpreadsheetReader reader, string filePath) : this(filePath)
        {
            _reader = reader;
        }

        public IFireEmblemCharacterGraph BuildGraph()
        {
            var graph = new FireEmblemCharacterGraph();
            foreach (var sheet in _sheets) // For each sheet
            {
                var columns = _reader.GetMaxColumnCount(sheet); 
                var rows = _reader.GetMaxRowCount(sheet);

                for (int currentRow = 1; currentRow <= rows; currentRow++) // Starts at first row
                {
                    try
                    {
                        var mainCharacterCell = _reader.GetCellStringValue(currentRow, 1);
                        if (mainCharacterCell != null)
                        {
                            var mainCharacterName = mainCharacterCell.ToString();
                            var mainCharacter = GetNodeFromGraph(mainCharacterName, currentRow, graph, sheet);
                            ProcessSupport_Internal(currentRow, columns, graph, mainCharacter, sheet);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error found while processing main character at row { currentRow }");
                        throw;
                    }
                }

            }

            return graph;
        }

        /// <summary>
        /// Generates supports that are references from the graph provided.
        /// </summary>
        /// <param name="currentRow"></param>
        /// <param name="columns"></param>
        /// <param name="sheet"></param>
        /// <param name="graph"></param>
        /// <param name="mainCharacter"></param>
        protected virtual void ProcessSupport_Internal(int currentRow, int columns, IFireEmblemCharacterGraph graph, IFireEmblemCharacterNode mainCharacter, string sheet)
        {
            for (int currentColumn = 2; currentColumn <= columns; currentColumn++) // Starts at second column
            {
                try
                {
                    var supportCharacterName = _reader.GetCellStringValue(currentRow, currentColumn, sheet);
                    if (supportCharacterName.Equals(END_OF_RELATIONSHIPS)) // If the column reached has the END_OF_RELATIONSHIPS, move to next row. This is a property of the source data.
                        break;
                    var supportCharacter = GetNodeFromGraph(supportCharacterName, currentRow, graph, sheet);
                    var hasSupport = mainCharacter.HasRelatedCharacter(supportCharacterName);
                    if (!hasSupport)
                        mainCharacter.AddRelationship(supportCharacter);
                    UpdateSupportCharacter(supportCharacter, sheet);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error found while processing support character at [{ currentRow }, { currentColumn }]");
                    throw;
                }
            }
        }

        protected virtual IFireEmblemCharacterNode CreateCharacter_Internal(string name, int currentRow, string sheet)
        {
            return CreateDefaultCharacterNode(name, sheet);
        }

        protected abstract void UpdateSupportCharacter(IFireEmblemCharacterNode characterNode, string sheet);

        protected IFireEmblemCharacterNode GetNodeFromGraph(string name, int currentRow, IFireEmblemCharacterGraph graph, string sheet)
        {
            if (!graph.HasCharacter(name))
            {
                var newCharacter = CreateCharacter_Internal(name, currentRow, sheet);
                graph.AddNode(newCharacter);
            }
            return graph.GetCharacter(name);
        }

        protected IFireEmblemCharacterNode CreateDefaultCharacterNode(string name, string sheet)
        {
            var newCharacterData = new FireEmblemCharacterData(name);
            var newCharacter = new FireEmblemCharacterNode(newCharacterData);
            return newCharacter;
        }

        public void Dispose()
        {
            _reader.Dispose();
        }
    }
}
