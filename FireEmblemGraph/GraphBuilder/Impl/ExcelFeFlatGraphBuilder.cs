﻿using FireEmblemGraph.Nodes.Interface;

namespace FireEmblemGraph.GraphBuilder.Impl
{
    public class ExcelFeFlatGraphBuilder : ExcelFeFlatGraphBuilderBase
    {
        public ExcelFeFlatGraphBuilder(string filePath) : base(filePath)
        {
        }

        protected override int StartRelationshipsColumn => 2;

        protected override void UpdateSupportCharacter(IFireEmblemCharacterNode characterNode, string sheet)
        {
            // Do nothing
        }
    }
}
