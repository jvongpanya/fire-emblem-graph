﻿using FireEmblemGraph.Domain.Impl;

namespace FireEmblemGraph.GraphBuilder.Interface
{
    public interface IFireEmblemGraphBuilderFactory
    {
        IFireEmblemCharacterGraphBuilder Create(EnumFireEmblemGames game);
    }
}
