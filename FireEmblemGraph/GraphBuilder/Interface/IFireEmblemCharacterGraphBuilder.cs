﻿using FireEmblemGraph.Graphs.Interface;
using System;

namespace FireEmblemGraph.GraphBuilder.Interface
{
    public interface IFireEmblemCharacterGraphBuilder : IDisposable
    {
        IFireEmblemCharacterGraph BuildGraph();
    }
}
