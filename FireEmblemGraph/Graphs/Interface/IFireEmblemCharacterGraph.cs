﻿using FireEmblemGraph.Nodes.Interface;
using System.Collections.Generic;

namespace FireEmblemGraph.Graphs.Interface
{
    public interface IFireEmblemCharacterGraph
    {
        IList<IFireEmblemCharacterNode> Nodes { get; }
        bool AddNode(IFireEmblemCharacterNode fireEmblemCharacterNode);
        bool HasCharacter(string name);
        IFireEmblemCharacterNode GetCharacter(string name);
    }
}
