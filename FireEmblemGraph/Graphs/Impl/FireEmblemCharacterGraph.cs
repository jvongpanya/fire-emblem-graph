﻿using FireEmblemGraph.Graphs.Interface;
using FireEmblemGraph.Nodes.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FireEmblemGraph.Graphs.Impl
{
    public class FireEmblemCharacterGraph : IFireEmblemCharacterGraph
    {
        public IList<IFireEmblemCharacterNode> Nodes { get; private set; }

        public FireEmblemCharacterGraph()
        {
            Nodes = new List<IFireEmblemCharacterNode>();
        }

        public bool AddNode(IFireEmblemCharacterNode fireEmblemCharacterNode)
        {
            var exists = Nodes.Any(n => n.CharacterData.Name.Equals(fireEmblemCharacterNode.CharacterData.Name));
            if (exists)
                return false;
            Nodes.Add(fireEmblemCharacterNode);
            return true;
        }

        public bool HasCharacter(string name)
        {
            return Nodes.Any(n => n.CharacterData.Name.Equals(name));
        }

        public IFireEmblemCharacterNode GetCharacter(string name)
        {
            if(HasCharacter(name))
                return Nodes.FirstOrDefault(n => n.CharacterData.Name.Equals(name));
            throw new ArgumentException($"The character, { name }, does not exist in this graph.");
        }
    }
}
