﻿namespace FireEmblemGraph.Domain.Impl
{
    public class FireEmblemThreeHousesCharacterData : FireEmblemCharacterDataBase
    {
        public string House { get; set; }
        public FireEmblemThreeHousesCharacterData(string name, string house) : base(name)
        {
            House = house;
        }

        public FireEmblemThreeHousesCharacterData(string name) : base(name) { }
    }
}
