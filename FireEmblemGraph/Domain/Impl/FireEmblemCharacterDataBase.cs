﻿using FireEmblemGraph.Domain.Interface;

namespace FireEmblemGraph.Domain.Impl
{
    public abstract class FireEmblemCharacterDataBase : ICharacterData
    {
        public string Name { get; private set; }
        protected FireEmblemCharacterDataBase(string name)
        {
            Name = name;
        }
    }
}
