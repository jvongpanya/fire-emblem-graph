﻿namespace FireEmblemGraph.Domain.Interface
{
    public interface ICharacterData
    {
        string Name { get; }
    }
}
